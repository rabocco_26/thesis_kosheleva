     # -*- coding: utf-8-*-
import math
import codecs
vowel = u"aeuyoióęą"

combo = [u"ia", u"ie", u"iu", u"iy", u"io", u"ią", u"ię"]

splitter = u".!?"

def count(str):
    res = 0
    oldres = -1
    while (oldres != res):
        oldres = res
        for c in combo:
            if c in str:
                str = str.replace(c, "", 1)
                res = res + 1
    for c in str:
        if (c in vowel):
            res += 1
    return res

def analyze_text(text):
    f = codecs.open(text, encoding="utf-8")
    t = ""
    for line in f:
        t += " " + line
    words = t.split(" ")
    total_len = 0
    seq_num = 0
    dif_num = 0
    for word in words:
        if (word == ""):
            continue
        total_len += 1
        for s in splitter:
            if s in word:
                seq_num += 1
                break
        if (count(word) > 3):
            dif_num += 1
    return math.sqrt((total_len*1.0/seq_num)**2 + (dif_num * 100.0/total_len)**2)/2

def run():
    texts = ["1.txt", "2.txt", "3.txt", "4.txt", "5.txt",
             "6.txt", "7.txt", "8.txt", "9.txt", "10.txt",
             "11.txt", "12.txt", "13.txt", "14.txt", "15.txt",
             "16.txt", "17.txt", "18.txt", "19.txt", "20.txt"]
    for text in texts:
        val = analyze_text("t" + text)
        print text, val

run()












