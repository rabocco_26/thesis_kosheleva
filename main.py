import codecs
import re
import math
def load_freq():
    d = {}
    f = codecs.open("freq.txt", encoding="utf-8")
    fl = True
    for line in f:
        if (fl):
            fl = False
            continue
        a = line.split(";")
        d[a[0]] = int(a[1].strip())
    return d

def analyze_text(freq, text):
    f = codecs.open(text, encoding="utf-8")
    d = {}
    for line in f:
        res = re.search(u"\\[(.*),(.*),(.*),(.*),(.*),(.*),(.*)\\]", line)
        if res.group(5).strip() == "interp":
            continue
        if res.group(4).strip() not in freq:
            continue
        v = math.log(43224589 / freq[res.group(4).strip()])
        if (res.group(1) not in d):
            d[res.group(1)] = v
        else:
            if (d[res.group(1)] > v):
                d[res.group(1)] = v
    sum = 0
    for elem in d.values():
        sum += elem
    return sum / len(d.values())

def run():
    texts = ["01.txt", "02.txt", "03.txt", "04.txt", "05.txt",
             "06.txt", "07.txt", "08.txt", "09.txt", "10.txt"]
    freq = load_freq()
    for text in texts:
        val = analyze_text(freq, text)
        print text, val

if __name__ == "__main__":
    run()
